#!/usr/bin/env python

import orderConditions.trees as t
t.generate_trees(5)
for i in range(11):
   with open(f'html/orderConditions/t_{i}.svg', 'wb') as f:
      f.write(t.plot(i, format='svg', dotWidth=.1).pipe())