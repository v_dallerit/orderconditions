#!/usr/bin/env python

import doctest
from orderConditions import trees
from orderConditions import bicolor_trees
from orderConditions import BSeries
from orderConditions import epi

doctest.testmod(trees, verbose=True, optionflags=doctest.NORMALIZE_WHITESPACE)
doctest.testmod(bicolor_trees, verbose=True, optionflags=doctest.NORMALIZE_WHITESPACE)
doctest.testmod(BSeries, verbose=True, optionflags=doctest.NORMALIZE_WHITESPACE)
doctest.testmod(epi, verbose=True, optionflags=doctest.NORMALIZE_WHITESPACE)
