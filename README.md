# orderConditions

orderConditions is a Python 3 package used to assist the derivation of order conditions for ordinary differential equation (ODE) numerical methods. 

It contains modules to manipulate rooted trees and B-Series as well as helper functions to derive certain class of schemes.

## Documentation 
The documentation is available on [https://v_dallerit.gitlab.io/orderconditions/](https://v_dallerit.gitlab.io/orderconditions/)