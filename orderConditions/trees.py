"""
This module contains functions to manipulate and compute properties of rooted trees.

To use this module, it is necessary to first generate the necessary trees with the `generate_trees` function.

It is then possible to:

- Access to trees indicies (`indices`)
- Manipulate trees using the *circ* prduct (`left`, `right`, `decompose`, `circ`)
- Manipulate trees using the *bracket* product (`children`, `from_children`)
- Get trees properties (cf. `order`, `gamma`, `sigma`)
- Plot the trees (`plot`)

All the examples in the documentation assume that the trees up to order 5 have been generated using:
>>> generate_trees(5)


For more details about the theory and useful definitions, the reader can refer to the following references:

- [J. C. Butcher, Numerical methods for ordinary differential equations (2016)](https://doi.org/10.1002/9781119121534)

- [J. C. Butcher, An algebraic theory of integration methods,Math. Comput.26(1972), 79–106](https://doi.org/10.2307/2004720)

"""

from itertools import product
from math import ceil, factorial
from collections import Counter
from bisect import bisect
from functools import reduce
from graphviz import Graph
from functools import lru_cache
from typing import Tuple, Mapping, Iterable

class _Data:
   def __init__(self):
      self.comp = [(0,0)]
      self.order_max = 0
      self.index_first = [0, 1]


# --------------------------------
#       TREE GENERATION
# --------------------------------
def generate_trees(order: int):
   """ Generate all the trees up to the specified order.
   This function must be run before accessing the properties of the trees.

   Internally, this function only stores:

   * Left and right trees in the circ product i.e. \\(t_i = \\text{left}(t_i) \\circ \\text{right}(t_i)\\)
   * Index of the first tree of each order

   To generate the trees up to order 5:
   >>> generate_trees(5)
   """
   if order > _data.order_max + 1:
      generate_trees(order - 1)
   
   if order > _data.order_max :
      for j in range(1, ceil(order/2)):
         idxLeft  = indices(order - j)
         idxRight = indices(j)
         for (l,r) in product(idxLeft, idxRight):
            if right(l) >= r:
               _data.comp.append((l,r))
               
      for r in indices(order - 1):
         _data.comp.append((1, r))

   _data.index_first.append(len(_data.comp))
   _data.order_max = order

# --------------------------------
#       TREE INDEXING
# --------------------------------
def indices(order: int) -> range:
   """ Return the index for all the trees of specified order.

   For example, all the trees of order 4 are: \\(\\{t_5, t_6, t_7, t_8\\}\\)
   >>> list(indices(4))
   [5, 6, 7, 8]
   """
   return range(_data.index_first[order], _data.index_first[order + 1])

# --------------------------------
#       CIRC PRODUCT
# --------------------------------
def left(i: int) -> int:
   """ Return the index of \\(t_i\\)'s left tree in the circ product:
   $$t_i = \\text{left}(t_i) \\circ \\text{right}(t_i)$$
   For example we have \\(t_4 = t_1 \\circ t_2 \\) so:
   >>> left(4)
   1
   """
   return _data.comp[i][0]

def right(i: int) -> int:
   """ Return the index of \\(t_i\\)'s right tree in the circ product:
   $$t_i = \\text{left}(t_i) \\circ \\text{right}(t_i)$$

   For example we have \\(t_4 = t_1 \\circ t_2 \\) so:
   >>> right(4)
   2
   """
   return _data.comp[i][1]

def decompose(i: int) -> Tuple[int, int]:
   """ Return a tuple containing the indices of \\(t_i\\)'s left and right trees in the circ product:
      $$t_i = \\text{left}(t_i) \\circ \\text{right}(t_i)$$

      For example we have \\(t_4 = t_1 \\circ t_2 \\) so:
      >>> decompose(4)
      (1, 2)
      """
   return _data.comp[i]

@lru_cache(maxsize = None)
def circ(l: int, r: int) -> int:
   """Return the result of the circ product of `l` and `r`.

   For example we have \\(t_4 = t_1 \\circ t_2 \\) so:
   >>> circ(1, 2)
   4
   """
   sum_order = order(l) + order(r)
   try:
      ans = _data.comp.index((l,r), _data.index_first[sum_order], _data.index_first[sum_order + 1])
   except ValueError:
      ans = from_children(children(l) + Counter([r]))

   return ans

# --------------------------------
#       BRACKET PRODUCT
# --------------------------------
def children(i: int) -> Mapping[int, int]:
   """ Return the trees obtained after removing the root of \\(t_i\\).

   The result is returned as a dictionary where the keys are the index of the tree and the values are the number of times the tree is present.
   It is possible to determine how many times a tree is present by using the `[]` operator.

   For example: \\(t_{10} = [{t_1}^2 \\ t_2]\\) so
   >>> c = children(10)
   >>> print(c)
   Counter({1: 2, 2: 1})
   >>> c[1] # t_1 is there twice
   2
   >>> c[3] # t_3 is not there
   0
   """
   if i < 2: return Counter()
   (l, r) = _data.comp[i]
   return children(l) + Counter([r])

def from_children(c) -> int:
   """ Return the index of the tree obtained by joining all the trees in `c` to a common root.
   This function is the inverse of the children function.

   `c` can be provided as a list or a dictionary.

   For example \\(t_{10} = [{t_1}^2 \\ t_2]\\) so:
   >>> from_children([1, 1, 2])
   10
   >>> from_children({1: 2, 2: 1}) # t_1 twice, t_2 once
   10
   """
   if not isinstance(c, Counter): c = Counter(c)
   return reduce(circ, sorted(c.elements(), reverse = True), 1)

# --------------------------------
#       MERGE ROOT
# --------------------------------
def merge_root(u: int, v:int) -> int: 
   """ Return the trees obtained by merging the roots of the tree `u` and `v`.

   For example \\(t_{3}\\) can be obtained by merging the roots of the trees \\(t_2\\) and \\(t_3\\).
   >>> c = merge_root(2,3)
   5
   """
   return from_children(children(u) + children(v))
    

# --------------------------------
#       TREE PROPERTIES
# --------------------------------
# Order
def order(i: int) -> int:
   """Return the order of the tree \\(t_i\\).

   For example \\(|t_5| = 4\\):
   >>> order(5)
   4
   """
   return bisect(_data.index_first, i) - 1

# Density
def gamma(i: int) -> int:
   """Return the density of the tree \\(t_i\\).

   The density of the tree \\(t = [{t_1}^{m_1} {t_2}^{m_2} ... {t_k}^{m_k}]\\) is computed using the formula:
   $$ \\gamma(t) = |t| \\prod_{i=1}^k \\gamma(t_i)^{m_i}$$
   For more details see [J. C. Butcher, Numerical methods for ordinary differential equations (2016)](https://doi.org/10.1002/9781119121534).

   For example \\(\\gamma(t_5) = 4\\):
   >>> gamma(5)
   4
   """
   if i < 2: return 1
   g = order(i)
   for c in children(i).items():
      g *= gamma(c[0]) ** c[1]
   return g

# Symmetry
def sigma(i: int) -> int:
   """Return the symmetry of the tree \\(t_i\\).

   The symmetry of the tree \\(t = [{t_1}^{m_1} {t_2}^{m_2} ... {t_k}^{m_k}]\\) is computed using the formula:
   $$ \\sigma(t) = \\prod_{i=1}^k m_i! \\ \\sigma(t_i)^{m_i}$$
   For more details see [J. C. Butcher, Numerical methods for ordinary differential equations (2016)](https://doi.org/10.1002/9781119121534).

   For example \\(\\sigma(t_5) = 6\\):
   >>> sigma(5)
   6
   """
   if i < 2: return 1
   s = 1
   for c in children(i).items():
      s *= factorial(c[1]) * sigma(c[0])**c[1]
   return s

# --------------------------------
#           DISPLAY
# --------------------------------
def _getDot(tree, dot, parent = None, idx = 1):
   idx_cur = idx
   dot.node(str(idx_cur), '')
   if parent:
      dot.edge(str(parent), str(idx_cur))

   for c in children(tree).elements():
      idx = _getDot(c, dot, idx_cur, idx+1)

   return idx

def plot(i: int, format = 'pdf', dotWidth = .2, hSpace = 1, vSpace = 1.5, fillcolor = 'grey', color = 'black', edgecolor = 'black', dotBorder = 1, edgeWidth = 1):
   """Display the tree \\(t_i\\)

   This function returns a graphviz Graph object. It is displayed directly inside a Jupyter notebook or can be viewed using the `view()` method.
   For more details see: https://graphviz.readthedocs.io.
   For example, the tree \\(t_{10}\\) will be represented as:
   ```
   plot(10)
   ```
   ![](t_10.svg)
   """
   dot = Graph(format=format)
   dot.attr(rankdir='BT')
   dot.attr('node', shape = 'circle', style = 'filled', width = str(dotWidth), penwidth = str(dotBorder), fillcolor = fillcolor, color = color)
   dot.attr('edge', color = edgecolor, penwidth = str(edgeWidth))
   dot.attr('graph', ranksep = str(dotWidth * vSpace), nodesep = str(dotWidth * hSpace))
   if i>0:
      _getDot(i, dot)
   return dot

# ------------------------------
#       Internal data
# ------------------------------
def clear():
   """ Remove all previously generated trees."""
   global _data
   _data = _Data()
   circ.cache_clear()

_data = _Data()


if __name__ == '__main__':
   import doctest
   generate_trees(5)
   doctest.testmod(verbose=True)
