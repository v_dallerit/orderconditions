"""
This module contains functions to manipulate and generate B-Series.

To use this module, it is necessary to first call the function `set_order`.
After this, the B-Series generated will have coefficients for all trees up to the desired order.

It is then possible to:

- Generate common B-Series (`exact`, `y`, `yN`, `hf`, `hr`)
- Apply function of the Jacobian to a B-Series (`fJac`)
- Compute the composition of B-Series (`compo`, `compo_hf`)
- Get the order condition and error terms (`order_conditions`, `error`, `get_order`)
- Group B-Series coefficients by order (`group_by_order`)
- Apply expression to B-Series coefficients (`apply_expr`)
- Find B-Series defined by implicit relation (`implicit_BSeries`)
- Display B-Series coefficients alongside with the corresponding trees (`display`)

For all the examples in this documentation, if no `set_order` command is present, it is assumed that the order is set to 4. This can be done by calling:
>>> set_order(4)

## Examples

This section contains simplified examples to show how one can derive order conditions for a numerical scheme using this module.

### 2-stage explicit Runge-Kutta

We consider the following 2-stage explicit Runge-Kutta method:
$$\\begin{aligned}
Y_1 &= y_n + a_{21} hf(y_n) \\\\
y_{n+1} &= y_n + b_1 hf(y_n) + b_2 hf(Y_1)
\\end{aligned}$$

We want to determine the conditions on \\(a_{21}, b_1, b_2\\) to get a second order method.

>>> from sympy import symbols, solve
>>> set_order(4)
>>> a_21 = symbols('a_21')
>>> (b_1,b_2) = symbols('b_(1:3)')
>>> Y1 = y() + a_21 * hf()
>>> rk2 = y() + b_1 * hf() + b_2 * compo_hf(Y1)
>>> oc = solve(order_conditions(rk2, 2), [b_1, b_2])
>>> print(oc)
{b_2: 1/(2*a_21), b_1: (a_21 - 1/2)/a_21}
>>> rk2 = apply_expr(rk2, oc)
>>> get_order(rk2)
2
>>> print(error(rk2, 3))
[a_21/2 - 1/3 -1/6]

As expected, the two conditions are \\(b_1 = \\frac{a_{21} - 1/2}{a_{21}} = 1 - \\frac{1}{2 a_{21}}\\) and \\(b_2 = \\frac{1}{2 a_{21}}\\).
We also got the \\(3^{rd}\\) order B-Series coefficients for the error: \\(\\left[\\frac{a_{21}}{2} - \\frac{1}{3}, -\\frac{1}{6}\\right]\\)

### Theta method

We start with the following ansatz for our numerical scheme:
$$ y_{n+1} = \\alpha y_n + \\beta h f(y_n) + \\gamma h f(y_{n+1})$$
and we want to determine the order conditions on \\(\\alpha, \\beta \\text{, and } \\gamma \\).
Because this scheme is implicit, we need to use the `implicit_BSeries` function.

>>> from sympy import symbols
>>> set_order(4)
>>> (a,b,c) = symbols('a b c')
>>> theta = implicit_BSeries(lambda x: x - a * y() - b * hf() - c * compo_hf(x))
>>> print(solve(order_conditions(theta, 1)))
{b: 1 - c, a: 1}
>>> print(solve(order_conditions(theta, 2)))
[{a: 1, b: 1/2, c: 1/2}]

As expected, we can see that:

 - For \\(1^{st}\\) order, we need \\( \\alpha = 1\\) and \\(\\beta + \\gamma = 1\\)
 - For \\(2^{nd}\\) order, we need \\( \\alpha = 1, \\beta = \\gamma = \\frac{1}{2} \\) which corresponds to the trapezoid method.

## References
For more details the reader can refer to the following references:

- [J. C. Butcher, Numerical methods for ordinary differential equations (2016)](https://doi.org/10.1002/9781119121534)

- [J. C. Butcher, Trees, B-series and exponential integrators, IMA Journal of Numerical Analysis, Volume 30, Issue 1, January 2010, Pages 131–140](https://doi.org/10.1093/imanum/drn086)

- [M. Tokman,
A new class of exponential propagation iterative methods of Runge–Kutta type (EPIRK), Journal of Computational Physics, Volume 230, Issue 24, 2011, Pages 8762-8778](https://doi.org/10.1016/j.jcp.2011.08.023)

- [J. C. Butcher, An algebraic theory of integration methods,Math. Comput.26(1972), 79–106](https://doi.org/10.2307/2004720)

- [J. C. Butcher, T.M.H. Chan, A New Approach to the Algebraic Structures for Integration Methods. BIT Numerical Mathematics 42, 477–489 (2002)](https://link.springer.com/article/10.1023/A:1021921008328)

- [P. Chartier, E. Hairer, & G. Vilmart, Algebraic Structures of B-series. Found Comput Math 10, 407–427 (2010)](https://doi.org/10.1007/s10208-010-9065-1)

"""
from IPython.display import HTML
from numpy import zeros, array, all, ndarray
from sympy import Rational, Integer, symbols, solve, factorial
from collections import defaultdict
from itertools import product
from . import trees as t

class _Data:
   def __init__(self):
      self.order_max = 0
      self.len = 1
      self.lambda_cache = [{}]

def set_order(order: int):
   """ Set the order to be used for the B-Series.
   This function must be run first to set the size of
   the B-Series and generate associated trees.

   To use B-Series with trees up to order 4:
   >>> set_order(4)
   """
   t.generate_trees(order)
   _data.order_max = order
   _data.len = t.indices(order).stop

# ------------------------------
#       Special B-Series
# ------------------------------
def exact():
   """ Return the B-Series of the exact solution at time \\(t_{n+1}: y_{n+1}\\)

   >>> print(exact())
   [1 1 1/2 1/3 1/6 1/4 1/8 1/12 1/24]
   """
   return array([Rational(1, t.gamma(i)) for i in range(_data.len)])

def y():
   """ Return the B-Series of the exact solution at time \\(t_{n}: y_{n}\\)

   >>> print(y())
   [1 0 0 0 0 0 0 0 0]
   """
   yn = zeros(_data.len, dtype=object)
   yn[0] = Integer(1)
   return yn

def yN(k):
   """ Return the B-Series of the exact solution at time \\(t_{n+k}\\)
   The value of k can be an integer, a real number or a sympy Symbol.

   * B-Series of \\(y_{n-1}\\):
   >>> print(yN(-1))
   [1 -1 1/2 -1/3 -1/6 1/4 1/8 1/12 1/24]

   * B-Series of \\(y_{n+1/3}\\):
   >>> import sympy
   >>> print(yN(sympy.Rational(1,3)))
   [1 1/3 1/18 1/81 1/162 1/324 1/648 1/972 1/1944]

   * B-Series of \\(y_{n+k}\\) for an arbitrary k:
   >>> k = sympy.symbols('k')
   >>> print(yN(k))
   [1 k k**2/2 k**3/3 k**3/6 k**4/4 k**4/8 k**4/12 k**4/24]
   """
   return array([k ** t.order(i) * Rational(1, t.gamma(i)) for i in range(_data.len)])

def hf():
   """ Return the B-Series of \\(h f(y_{n})\\)

   >>> print(hf())
   [0 1 0 0 0 0 0 0 0]
   """
   f = zeros(_data.len, dtype=object)
   f[1] = Integer(1)
   return f

def hr(z):
   """ Return the B-Series of the residual function evaluated at `z`: \\[ hr(z) = h f(z) - h f(y_n) - J_n (z - y_n) \\]

   For example, the B-Series of the residual function at the previous time step \\(r(y_{n-1})\\) is:
   >>> print(hr(yN(-1)))
   [0 0 0 1 0 -1 -1/2 0 0]
   """
   return compo_hf(z) - hf() - _jac(z - y())


# ------------------------------
#    Function of jacobian
# ------------------------------
def _jac(z):
   ans = zeros(_data.len, dtype=object)
   for tree in range(0, t.indices(_data.order_max).start):
      ans[t.circ(1,tree)] = z[tree]
   return ans

def fJac(f, z):
   """ Compute the B-Series of \\(f(hJ_n) z\\) where `f` is a matrix-function and `z` is a B-Series.

   The function `f` must be given as the first `n` coefficients of it's Taylor series around 0 where `n` is greater than the order set with `set_order`.

   For example, the following code computes the B-Series of \\(\\exp(hJ_n) y_{n}\\):
   >>> from sympy import factorial, Rational
   >>> f = [ Rational(1, factorial(n)) for n in range(5) ]
   >>> print(fJac(f,y()))
   [1 1 1/2 0 1/6 0 0 0 1/24]
   """
   ans = f[0] * z
   Jnz = z
   for i in range(_data.order_max):
      Jnz = _jac(Jnz)
      ans += f[i+1] * Jnz
   return ans

# ------------------------------
#      Composition rule
# ------------------------------
def _lambda_compo(alpha, tree):
   if tree == 0:
      return {}
   if tree == 1:
      return {1: Integer(1)}

   (u, v) = t.decompose(tree)
   lambda_u = _data.lambda_cache[u]
   lambda_v = _data.lambda_cache[v]

   ans = defaultdict(lambda: Integer(0))
   for ((tu, nu), (tv, nv)) in product(lambda_u.items(), lambda_v.items()):
      ans[t.circ(tu, tv)] += nu * nv
   for (tu, nu) in lambda_u.items():
      ans[tu] += nu * alpha[v]

   return ans


def compo(alpha, beta):
   """ Returns the composition of two B-Series.

   The result is computed using the following formula:
   $$ (\\alpha \\beta)(t) = \\lambda(\\alpha, t)(\\beta) + \\alpha(t) \\beta(\\emptyset)$$
   where \\( \\lambda(\\alpha, t_1) = \\widehat{t_1} \\) and
   $$ \\lambda(\\alpha, u\\circ v) =  \\lambda(\\alpha, u) \\circ \\lambda(\\alpha, v) + \\alpha(v) \\lambda(\\alpha, u) $$

   For more details see [J. C. Butcher, An algebraic theory of integration methods,Math. Comput.26(1972), 79–106](https://doi.org/10.2307/2004720).

   For example, the B-Series of \\(hf(y_{n-2})\\) is:
   >>> print(compo(yN(-2), hf()))
   [0 1 -2 4 2 -8 -4 -8/3 -4/3]

   Note that if the second argument is `hf()`, it is recommanded to use the `compo_hf` function as it is more efficient.
   """
   ans = zeros(_data.len, dtype=object)
   _data.lambda_cache = [{}] * _data.len
   for tree in range(_data.len):
      ans[tree] = alpha[tree] * beta[0]
      _data.lambda_cache[tree] = _lambda_compo(alpha, tree)
      for (t, n) in _data.lambda_cache[tree].items():
         ans[tree] += n * beta[t]
   return ans

def compo_hf(alpha):
   """ Return the B-Series of \\( hf(z) \\) where `z` is a B-Series.

   * B-Series of \\(hf(y_{n-2})\\):
   >>> print(compo_hf(yN(-2)))
   [0 1 -2 4 2 -8 -4 -8/3 -4/3]
   """
   ans = zeros(_data.len, dtype=object)
   ans[1] = Integer(1)
   for tree in range(2, _data.len):
      (u,v) = t.decompose(tree)
      ans[tree] = alpha[v] * ans[u]
   return ans


# ------------------------------
#      Substitution rule
# ------------------------------
def _lambda_sub():
    l = [ defaultdict(list) for _ in range(_data.len)]
    l[0][0] = [[]]
    l[1][1] = [[1]]

    for tree in range(2,_data.len):
        (u,v) = t.decompose(tree)
        for ((tu, list_fu), (tv, list_fv)) in product(l[u].items(), l[v].items()):
            circ_uv = t.circ(tu, tv)
            merge_uv = t.merge_root(tu, tv)
            for (fu, fv) in product(list_fu, list_fv):
                l[tree][circ_uv].append(fu + fv)
                l[tree][merge_uv].append([t.circ(fu[0],fv[0])] + fu[1:] + fv[1:])

    return l

def substitution(a, b):
    l = _lambda_sub()
    
    ans = zeros(_data.len, dtype = object)
    for i in range(_data.len):
        for (b_tree, list_forest) in l[i].items():
            for forest in list_forest:
                k = 1
                for a_tree in forest:
                    k *= a[a_tree]
                ans[i] += k *  b[b_tree]
    return ans

def modified_equation(a):
    e = exact()
    series = zeros(_data.len, dtype=object)
    series[1] = a[1]
    l = _lambda_sub()

    for t in range(2,_data.len):
        tmp = 0
        for (b_tree, list_forest) in l[t].items():
            for forest in list_forest:
                k = 1
                for a_tree in forest:
                    k *= series[a_tree]
                tmp += k *  e[b_tree]
        series[t] = a[t] - tmp

    return series
        
def modifying_integrator(a):
    e = exact()
    series = zeros(_data.len, dtype=object)
    series[1] = a[1]
    l = _lambda_sub()

    for t in range(2,_data.len):
        tmp = 0
        for (b_tree, list_forest) in l[t].items():
            for forest in list_forest:
                k = 1
                for a_tree in forest:
                    k *= series[a_tree]
                tmp += k *  a[b_tree]
        series[t] = e[t] - tmp
        
    return series

# ------------------------------
#      Order conditions
# ------------------------------

def order_conditions(z, order = None):
   """ Return the order conditions associated with the B-Series `z` for the given `order`.
   If the `order` is not provided, the default value is the order set with `set_order`.

   For example, the \\(1^{st}\\) order conditions for the method:
   $$ y_{n+1} = \\alpha  y_n + \\beta  hf(y_n) $$
   are \\(\\alpha = \\beta = 1\\):
   >>> from sympy import symbols, solve
   >>> (a,b) = symbols('a b')
   >>> oc = order_conditions(a * y() + b * hf(), 1)
   >>> print(oc)
   [a - 1 b - 1]
   >>> print(solve(oc))
   {a: 1, b: 1}
   """
   if order is None:
      order = _data.order_max

   return (z - exact())[:t.indices(order).stop]

def error(z, order=None):
   """ Return the error coefficients of order `order` associated with the B-Seires `z`.
   If the `order` is not provided, the default value is the order set with `set_order`.

   For example, the error coefficient of second order for the explicit Euler method is:
   >>> print(error(y() + hf(), 2))
   [-1/2]
   """
   if order is None:
      order = _data.order_max

   return (z - exact())[t.indices(order)]

def get_order(z):
   """ Return the order of the method represented by the B-Series `z`.

   For example, we can check that the explicit Euler method is \\(1^{st}\\) order:
   >>> get_order(y() + hf())
   1

   Note that the value returned by this function will always be at most equal to the order set with `set_order`.
   """
   order = 0
   while (order <= _data.order_max and all((z - exact())[t.indices(order)] == 0)):
      order += 1
   return order-1

# ------------------------------
#            Misc
# ------------------------------


def group_by_order(z):
   """ Group the coefficients of the B-Series `z` by order.
   This function returns a vector of vectors where the \\(i^{th}\\) element contains all the coefficients of order \\(i\\).

   For example, with the exact solution \\(y_{n+1}\\), we have:
   >>> for coeff in group_by_order(exact()):
   ...   print(coeff)
   [1]
   [1]
   [1/2]
   [1/3 1/6]
   [1/4 1/8 1/12 1/24]
   """
   ans = []
   for order in range(_data.order_max+1):
      ans.append(z[t.indices(order)])
   return array(ans, dtype=object)

def apply_expr(z, expr):
   """ Apply the expressions contained in `expr` to the B-Series `z`.

   Internally, it applies the [sympy's `subs`](https://docs.sympy.org/latest/modules/core.html?highlight=subs#sympy.core.basic.Basic.subs) function to each coefficient.

   For example, the following code replaces the variable \\(a\\) and \\(b \\) with \\(1\\) in the B-Series of \\(a y_{n} + b hf(y_n)\\):
   >>> from sympy import symbols
   >>> (a,b) = symbols('a b')
   >>> bs = a * y() + b * hf()
   >>> print(bs)
   [a b 0 0 0 0 0 0 0]
   >>> print(apply_expr(bs, {a: 1, b: 1}))
   [1 1 0 0 0 0 0 0 0]
   """
   if isinstance(z[0], (list,ndarray)):
      return array([apply_expr(v,expr) for v in z])
   else:
      return array([v.subs(expr).simplify() for v in z])

def implicit_BSeries(f):
   """ Find the B-Series of `x` defined by the implicit relation \\(f(x) = 0\\).
   The function `f` can be provided as a regular function or a lambda function.

   For example, the B-Series of the implicit Euler method can be found using the following code:
   >>> def imp_euler(x):
   ...   return x - y() - compo_hf(x)
   >>> print(implicit_BSeries(imp_euler))
   [1 1 1 1 1 1 1 1 1]
   >>> print(implicit_BSeries(lambda x: x - y() - compo_hf((x))))
   [1 1 1 1 1 1 1 1 1]
   """
   x = symbols(f'x_(:{_data.len})')
   return array(solve(f(x),x)[0])

# ------------------------------
#          Display
# ------------------------------
def display(z, idx=None):
   """ Display the B-Series coefficients of `z` alongside with the correponding trees.
   If `idx` is not provided, the function assumes that the coefficients correspond to the trees in order and strating from \\(0\\). Otherwise, `idx` should represent the index of the trees corresponding to each coefficient.

   Display exact solution coefficients:
   ```
   set_order(3)
   display(exact())
   ```
   Tree           | Coefficients
   -------------- |--------------
   ![](t_0.svg)   |     1
   ![](t_1.svg)   |     1
   ![](t_2.svg)   |     1/2
   ![](t_3.svg)   |     1/3
   ![](t_4.svg)   |     1/6

   Display only \\(4^{th}\\) order coefficients:
   ```
   from orderConditions.trees import indices
   set_order(4)
   display(group_by_order(exact())[4], indices(4))
   ```
   Tree           | Coefficients
   -------------- |--------------
   ![](t_5.svg)   |     1/4
   ![](t_6.svg)   |     1/8
   ![](t_7.svg)   |     1/12
   ![](t_8.svg)   |     1/24

   """
   if idx is None:
      idx = range(len(z))
   html_str = '<table><tr><th>Tree</th><th>Coefficient</th></tr>'
   for (c, tree) in zip(z, idx):
      html_str += f"<tr><td>{t.plot(tree, format='svg', dotWidth=.1).pipe().decode('UTF-8')}</td><td>{c}</td></tr>"
   html_str += '</table>'
   return HTML(html_str)

# ------------------------------
#       Internal data
# ------------------------------

def clear():
   """ Remove cached data related to B-Series."""
   _data = _Data()

_data = _Data()



if __name__ == '__main__':
   import doctest
   set_order(4)
   doctest.testmod(verbose=True)
