""" This module contains functions to generate coefficients for exponential multistep (EPI) methods.

The general ansatz for these methods is:
$$
\\begin{aligned}
y_{n+1} &= y_n + \\varphi_1(hJ_n)hf(y_n) + \\sum_{k=2}^K \\varphi_k(hJ_n) v_k \\\\
v_k &= \\sum_{i=1}^P \\alpha_{k,i} hR(y_{n-i})
\\end{aligned}
$$
where \\(P\\) is the number of previous points used and \\(K\\) is the maximum order of the \\(\\varphi\\) function.

The following functions can be used to find the coefficients \\(\\alpha_{k,i}\\) in order to construct specific EPI schemes.

For example, with the parameters \\(K = 3\\) and \\(P = 2\\), it is possible to get a \\(4^{th}\\) order method as follow:
>>> from orderConditions import BSeries as bs
>>> bs.set_order(5)
>>> K, P = 3, 2
>>> met = epi(K, P)
>>> coeff = get_coeff(met)
>>> bs.get_order(bs.apply_expr(met, coeff))
4
>>> print(coeff)
{a_21: -3/10, a_31: 32/5, a_22: 3/40, a_32: -11/10}
>>> print(export_coeff(coeff, 3, 2, 'python'))
[
[-3/10, 3/40],
[32/5, -11/10]
]
"""

from numpy import array
from sympy import Rational, factorial, symbols, solve
from . import BSeries as bs

def epi(K: int, P: int):
   """ Return the B-Series of the EPI scheme with parameters:

   - `P` the number of previous points used
   - `K` the maximum order of the \\(\\varphi\\) function.

   >>> from orderConditions import BSeries as bs
   >>> bs.set_order(3)
   >>> print(epi(2,2))
   [1 1 1/2 a_21/2 + 2*a_22 1/6]
   """
   a = array(symbols(f'a_(:{K+1})(:{P+1})')).reshape((K+1,P+1))
   v = lambda k: sum(a[k,i] * bs.hr(bs.yN(-i)) for i in range(1,P+1))
   return bs.y() + bs.fJac(phi(1), bs.hf()) + sum(bs.fJac(phi(k), v(k)) for k in range(2, K+1))

def get_coeff(met):
   """ Compute the coefficients for the EPI scheme `met`.
   This function maximizes the number of zero coefficients in the B-Series of the error.

   A dictionary is returned where the keys and the values correspond respectivelly to the coefficients names and values.

   >>> from orderConditions import BSeries as bs
   >>> bs.set_order(4)
   >>> print(get_coeff(epi(2,1)))
   {a_21: 2/3}
   """
   m = bs.group_by_order(met)
   var = {}
   e = bs.group_by_order(bs.exact())

   for order in range(bs._data.order_max+1):
      n = len(m[order])
      for idx in reversed(range(n)):
         if bool(m[order][idx].free_symbols):
            cond = solve(m[order][idx] - e[order][idx], dict = True)[0]
            var.update(cond)
            m = bs.apply_expr(m, cond)
   return {k:v.subs(var) for k,v in var.items()}

def export_coeff(expr, K, P, format = 'python'):
   """ Export the matrix of coefficients stored in `expr` for easy copy\paste.
    The format can be `Python`, `Matlab` or `Latex`.
   >>> from orderConditions import BSeries as bs
   >>> bs.set_order(5)
   >>> coeff = get_coeff(epi(3,2))
   >>> print(coeff)
   {a_21: -3/10, a_31: 32/5, a_22: 3/40, a_32: -11/10}
   >>> print(export_coeff(coeff, 3, 2,'python'))
   [
   [-3/10, 3/40],
   [32/5, -11/10]
   ]
   >>> print(export_coeff(coeff, 3, 2,'matlab'))
   [
   -3/10, 3/40;
   32/5, -11/10
   ]
   >>> print(export_coeff(coeff, 3, 2,'latex'))
   \\begin{pmatrix}
   -\\frac{3}{10} & \\frac{3}{40} \\\\[1em]
   \\frac{32}{5} & -\\frac{11}{10}
   \\end{pmatrix}
   """
   a = array(symbols(f'a_(:{K + 1})(:{P + 1})')).reshape((K + 1, P + 1))
   a = bs.apply_expr(a,expr)
   coeff = ''
   if format.lower() == 'python':
      coeff = '[\n'
      for k in range(2, K+1):
         coeff += '['
         for i in range(1,P+1):
            coeff += str(a[k,i]) + ', '
         coeff = coeff[:-2] + '],\n'
      coeff = coeff[:-2] + '\n]'
   if format.lower() == 'matlab':
      coeff = '[\n'
      for k in range(2, K + 1):
         for i in range(1, P + 1):
            coeff += str(a[k, i]) + ', '
         coeff = coeff[:-2] + ';\n'
      coeff = coeff[:-2] + '\n]'
   if format.lower() == 'latex':
      coeff = '\\begin{pmatrix}\n'
      for k in range(2, K + 1):
         for i in range(1, P + 1):
            if isinstance(a[k,i],Rational):
               if a[k,i] < 0:
                  coeff += '-'
               n,d = abs(a[k,i]).as_numer_denom()
               if d == 1:
                  coeff += str(n) + ' & '
               else:
                  coeff += f'\\frac{{{n}}}{{{d}}} & '
            else:
               coeff += str(a[k, i]) + ' & '
         coeff = coeff[:-2] + '\\\\[1em]\n'
      coeff = coeff[:-8] + '\n\\end{pmatrix}'
   return coeff

def phi(k, x = 1):
   """ Return the coefficients of the Taylor series around \\(z = 0\\) of \\(\\varphi_k(x z)\\) where the \\(\\varphi\\) functions are defined as:
   $$\\varphi_0(z) = e^z = \\sum_{n=0}^\\infty \\frac{z^n}{n!}$$
   $$\\varphi_k(z) = \\int_0^1 e^{(1-\\theta) z }\\frac{\\theta^{k-1}}{(k-1)!}d\\theta  = \\sum_{n=0}^\\infty \\frac{z^n}{(n+k)!}  \\ \\ \\text{for} \\ \\ k \\ge 1$$

   If `n` is the order set by `orderConditions.BSeries.set_order`, this function will return the `n+1` first coefficient of the function.
   The result of this function can be used directly as the first argument of the `orderConditions.BSeries.fJac` function.
   The second argument `x` is optional and set to 1 by default.

   * Taylor coefficients of \\( \\varphi_2(3z) \\):
   >>> from orderConditions.BSeries import fJac, hf, set_order
   >>> set_order(4)
   >>> print(phi(2,3))
   [1/2 1/2 3/8 9/40 9/80]

   * B-Series of \\( \\varphi_1(h J_n) hf_n\\):
   >>> print(fJac(phi(1), hf()))
   [0 1 1/2 0 1/6 0 0 0 1/24]
   """
   return array([x**n * Rational(1, factorial(n+k)) for n in range(bs._data.order_max + 1)])
